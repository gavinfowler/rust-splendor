use crate::splendor::card_set::CardSet;
use crate::splendor::gem_bank::GemBank;
use crate::splendor::noble_tile::NobleTile;

use super::gem::Gem;

pub struct Player {
    player_bank: GemBank,
    pub cards: CardSet,
    pub noble: Option<NobleTile>,
}

impl Player {
    pub fn points(&self) -> u8 {
        let mut points: u8 = 0;

        for card in &self.cards.cards {
            points += card.points;
        }

        match &self.noble {
            Some(x) => points += x.points,
            None => {}
        }

        points
    }

    pub fn add_gem(&mut self, gem: Gem) -> bool {
        let can_add: bool = self.player_bank.count() > 10;

        if can_add {
            self.player_bank.add(gem, 1);
        }

        return can_add;
    }

    pub fn get_player_bank_count(&self) -> u8 {
        self.player_bank.count()
    }
}
