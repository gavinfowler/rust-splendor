use rand::Rng;

use crate::splendor::gem::Gem;
use crate::splendor::gem_bank::GemBank;

#[derive(Debug, PartialEq)]
pub struct Card {
    pub points: u8,
    pub gem_type: Gem,
    pub cost: GemBank,
}

impl Card {
    pub fn test() {
        println!("Card Test");
    }

    pub fn create_lvl1_card() -> Card {
        let mut rng = rand::thread_rng();

        return Card {
            points: rng.gen_range(1..2) - 1,
            gem_type: rand::random(),
            cost: Self::card_gem_set(3),
        };
    }

    pub fn create_lvl2_card() -> Card {
        let mut rng = rand::thread_rng();

        return Card {
            points: rng.gen_range(1..2),
            gem_type: rand::random(),
            cost: Self::card_gem_set(5),
        };
    }

    pub fn create_lvl3_card() -> Card {
        let mut rng = rand::thread_rng();

        return Card {
            points: rng.gen_range(3..4),
            gem_type: rand::random(),
            cost: Self::card_gem_set(7),
        };
    }

    fn create_card(min_points: u8, max_points: u8, cost: u8) -> Card {
        let mut rng = rand::thread_rng();

        return Card {
            points: rng.gen_range(min_points..max_points),
            gem_type: rand::random(),
            cost: Self::card_gem_set(cost),
        };
    }

    fn card_gem_set(base_cost: u8) -> GemBank {
        let mut gs = GemBank::create();

        let mut rng = rand::thread_rng();

        let cost: u8 = base_cost + rng.gen_range(0..1);

        for _ in 0..cost {
            gs.add(rand::random(), 1)
        }

        gs
    }
}
