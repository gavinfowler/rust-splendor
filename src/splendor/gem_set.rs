use crate::splendor::gem::Gem;
use array_tool::vec::Uniq;

pub struct GemSet(Vec<Gem>);

impl GemSet {
    pub fn get_gems(self) -> Vec<Gem> {
        self.0
    }

    pub fn uniq(&self) -> bool {
        self.0.is_unique()
    }

    pub fn add(&mut self, gem: Gem) {
        self.0.push(gem);
    }
}
