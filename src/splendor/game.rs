use array_tool::vec::Uniq;

use crate::splendor::card::Card;
use crate::splendor::game_constants::*;
use crate::splendor::gem_bank::GemBank;
use crate::splendor::gem_set::GemSet;
use crate::splendor::noble_tile::NobleTile;
use crate::splendor::player::Player;

pub struct Game {
    pub tier_1_cards: Vec<Card>,
    pub tier_2_cards: Vec<Card>,
    pub tier_3_cards: Vec<Card>,
    pub game_bank: GemBank,
    pub nobles: Vec<NobleTile>,
    pub players: Vec<Player>,
}

impl Game {
    pub fn action_3_gems(gems: GemSet, player: &mut Player) -> bool {
        if gems.uniq() && ((player.get_player_bank_count() + 3) > MAX_PLAYER_GEMS) {
            // Player add gems
            // bank remove gems
        }
        true
    }

    pub fn transfer_noble(&mut self, _player: &mut Player) {
        println!("Transfer Noble");
    }
}
