use crate::splendor::gem_bank::GemBank;

pub struct NobleTile {
    pub points: u8,
    pub cost: GemBank,
}
