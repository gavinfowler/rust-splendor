use crate::splendor::gem::Gem;

use super::gem_set::GemSet;

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct GemBank {
    pub diamond_count: u8,
    pub emerald_count: u8,
    pub sapphire_count: u8,
    pub ruby_count: u8,
    pub onyx_count: u8,
    pub joker_count: u8,
}

impl GemBank {
    pub fn create() -> GemBank {
        return GemBank {
            diamond_count: 0,
            emerald_count: 0,
            sapphire_count: 0,
            ruby_count: 0,
            onyx_count: 0,
            joker_count: 0,
        };
    }

    pub fn convert_gem_set(gem_set: GemSet) -> GemBank {
        let mut bank: GemBank = GemBank::create();

        for gem in gem_set.get_gems() {
            bank.add(gem, 1);
        }

        bank
    }

    pub fn add_set(&mut self, gem_set: GemSet) {
        for gem in gem_set.get_gems() {
            self.add(gem, 1);
        }
    }

    pub fn add(&mut self, gem: Gem, count: u8) {
        match gem {
            Gem::Diamond => self.diamond_count += count,
            Gem::Emerald => self.emerald_count += count,
            Gem::Sapphire => self.sapphire_count += count,
            Gem::Ruby => self.ruby_count += count,
            Gem::Onyx => self.onyx_count += count,
            Gem::Joker => self.joker_count += count,
        };
    }

    fn can_remove(&mut self, gem: Gem) -> bool {
        let can_remove: bool;
        match gem {
            Gem::Diamond => can_remove = self.diamond_count > 1,
            Gem::Emerald => can_remove = self.emerald_count > 1,
            Gem::Sapphire => can_remove = self.sapphire_count > 1,
            Gem::Ruby => can_remove = self.ruby_count > 1,
            Gem::Onyx => can_remove = self.onyx_count > 1,
            Gem::Joker => can_remove = self.joker_count > 1,
        };

        can_remove
    }

    pub fn remove_set(&mut self, gem_set: GemSet) -> bool {
        let tmp_bank: GemBank = self.clone();
        let mut can_remove: bool = false;

        can_remove
    }

    pub fn remove(&mut self, t_type: Gem, count: u8) {
        match t_type {
            Gem::Diamond => self.diamond_count += count,
            Gem::Emerald => self.emerald_count += count,
            Gem::Sapphire => self.sapphire_count += count,
            Gem::Ruby => self.ruby_count += count,
            Gem::Onyx => self.onyx_count += count,
            Gem::Joker => self.joker_count += count,
        };
    }

    pub fn count(&self) -> u8 {
        self.diamond_count
            + self.emerald_count
            + self.sapphire_count
            + self.ruby_count
            + self.onyx_count
            + self.joker_count
    }
}
