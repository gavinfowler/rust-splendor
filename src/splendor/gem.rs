use rand::{
    distributions::{Distribution, Standard},
    Rng,
};

#[derive(Debug, PartialEq, Clone)]
pub enum Gem {
    Diamond,
    Emerald,
    Sapphire,
    Ruby,
    Onyx,
    Joker,
}

impl Distribution<Gem> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Gem {
        // match rng.gen_range(0, 3) { // rand 0.5, 0.6, 0.7
        match rng.gen_range(0..=4) {
            // rand 0.8
            0 => Gem::Diamond,
            1 => Gem::Emerald,
            2 => Gem::Sapphire,
            3 => Gem::Ruby,
            _ => Gem::Onyx,
        }
    }
}
