use crate::splendor::card::Card;
use crate::splendor::gem::Gem;

pub struct CardSet {
    pub cards: Vec<Card>,
}

impl CardSet {
    pub fn test() {
        println!("CardSet Test");
    }

    pub fn diamond_count(&self) -> u8 {
        let mut count: u8 = 0;
        for card in &self.cards {
            if card.gem_type == Gem::Diamond {
                count += 1;
            }
        }
        count
    }
    pub fn emerald_count(&self) -> u8 {
        let mut count: u8 = 0;
        for card in &self.cards {
            if card.gem_type == Gem::Emerald {
                count += 1;
            }
        }
        count
    }
    pub fn sapphire_count(&self) -> u8 {
        let mut count: u8 = 0;
        for card in &self.cards {
            if card.gem_type == Gem::Sapphire {
                count += 1;
            }
        }
        count
    }
    pub fn ruby_count(&self) -> u8 {
        let mut count: u8 = 0;
        for card in &self.cards {
            if card.gem_type == Gem::Ruby {
                count += 1;
            }
        }
        count
    }
    pub fn onyx_count(&self) -> u8 {
        let mut count: u8 = 0;
        for card in &self.cards {
            if card.gem_type == Gem::Onyx {
                count += 1;
            }
        }
        count
    }
}
